# aliases
alias ls='ls --color=auto'
alias mkdir='mkdir -pv'
alias mv='mv -iv'
alias cp='cp -rv'
alias v='vim'

# variables
export PS1='\W % ' 

# remove history
unset HISTORY

# blinking bar
echo -e -n "\x1b[\x35 q" # changes to blinking bar

# run shutdown-er
python3 $HOME/py/shutdown.py &

# launch x
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx
fi
